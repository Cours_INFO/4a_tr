/******************************************************************************/
/*			Application: flash					*/
/******************************************************************************/
/*									      */
/*			 programme  CLIENT				      */
/*									      */
/******************************************************************************/
/*									      */
/*		Auteurs : Bonfils Antoine, Ferrari Julien, Thomazo Corentin*/
/*		Date :  9 décembre 2022						      */
/*									      */
/******************************************************************************/	


#include <stdio.h>
#include <curses.h> 		/* Primitives de gestion d'ecran */
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <stdlib.h>
#include <string.h>

#include "../lib/fon.h"   		/* primitives de la boite a outils */

#define SERVICE_DEFAUT "1111"
#define SERVEUR_DEFAUT "127.0.0.1"

#define Respond_ok 48
#define Respond_Erreur 49


#define Type_sub 48
#define Type_unsub 49
#define Type_list 50
#define Type_publish 51
#define Type_connect 52
#define Type_disconnect 53

void client_appli (char *serveur, char *service);
void publish_message(int num_socket,char* pseudo);
void list_sub(int num_socket,char* pseudo);
void unfollow(int num_socket,char* pseudo);
void follow(int num_socket,char* pseudo);
void run(int num_socket,struct sockaddr_in *p_adr_client);
int envoie(int type,int num_socket,char* pseudo,char* data);
void authentification(char* pseudo,int);
int reception(char* pseudo,int num_socket);
void disconnect(char* pseudo,int num_socket);	



/*****************************************************************************/
/*--------------- programme client -----------------------*/

int main(int argc, char *argv[])
{

	char *serveur= SERVEUR_DEFAUT; /* serveur par defaut */
	char *service= SERVICE_DEFAUT; /* numero de service par defaut (no de port) */


	/* Permet de passer un nombre de parametre variable a l'executable */
	switch(argc)
	{
 	case 1 :		/* arguments par defaut */
		  printf("serveur par defaut: %s\n",serveur);
		  printf("service par defaut: %s\n",service);
		  break;
  	case 2 :		/* serveur renseigne  */
		  serveur=argv[1];
		  printf("service par defaut: %s\n",service);
		  break;
  	case 3 :		/* serveur, service renseignes */
		  serveur=argv[1];
		  service=argv[2];
		  break;
    default:
		  printf("Usage:client serveur(nom ou @IP)  service (nom ou port) \n");
		  exit(1);
	}

	/* serveur est le nom (ou l'adresse IP) auquel le client va acceder */
	/* service le numero de port sur le serveur correspondant au  */
	/* service desire par le client */
	
	client_appli(serveur,service);
}

/*****************************************************************************/

/**
 * Il crée un socket, se connecte au serveur, puis exécute le client
 * 
 * @param serveur l'adresse IP du serveur
 * @param service le numéro de port
 */
void client_appli (char *serveur,char *service) {	
	struct sockaddr_in* p_adr_client;
	adr_socket(service,serveur,SOCK_STREAM,&p_adr_client);
	int num_socket =h_socket(AF_INET,SOCK_STREAM);
	if (num_socket==-1) {
		printf("erreur cration socket");
		return ;
	}
	h_connect(num_socket,p_adr_client);
	run(num_socket,p_adr_client);
	h_close(num_socket);
}

/**
 * Il execute le corps du socket client
 * 
 * @param num_socket le numéro de prise
 * @param p_adr_client l'adresse du client
 */
void run(int num_socket,struct sockaddr_in *p_adr_client) {
	bool boucle=true;
	printf("\n\n############################################################\n");
	printf("# Bienvenue dans FLASH, le nouveau TWITTER (mais en mieux) #\n");
	printf("############################################################\n\n");
	printf("En créant cette socket, vous accès à FLASH.\n\n");
	printf("Pour utiliser cette application, vous devez entrer des caractères suivents pour interragir avec l'application\n");
	printf("	 p pour publier un message\n	 s pour vous abonner\n	 d pour vous désabonner\n	 l pour afficher la liste des abonnements\n	 q pour quitter l'application\n");
	char pseudo[6];
	authentification(pseudo,num_socket);
	
	fd_set rfds;
	fd_set rfdsbis;
	char buffer[255];

	struct timeval tv;
	int retval;

		/* Watch stdin (fd 0) to see when it has input. */

	FD_ZERO(&rfds);
	int maxsock= getdtablesize() ;
	FD_SET(0, &rfds);
	FD_SET(num_socket, &rfds);

	//FD_SET(num_socket,&rfds);
	bcopy ( (char*) &rfds, (char*) &rfdsbis, sizeof(rfds)) ;
	
	int k;
	fgets(buffer, sizeof(buffer), stdin);

	while (boucle) {
		k =select (FD_SETSIZE,&rfds, 0, 0, 0) ;
		if (k>0&&FD_ISSET(0, &rfds)) {
			fgets(buffer, sizeof(buffer), stdin);
			switch (buffer[0]) {	
				case 's':
					follow(num_socket,pseudo);	
					fgets(buffer, sizeof(buffer), stdin);
					break;	
				case 'd':	
					unfollow(num_socket,pseudo);
					fgets(buffer, sizeof(buffer), stdin);	
					break;	
				case 'l':	
					list_sub(num_socket,pseudo);	
					break;	
				case 'p':	
					publish_message(num_socket,pseudo);	
					fgets(buffer, sizeof(buffer), stdin);
					break;	
				case 'q':	
					boucle=false;
					disconnect(pseudo,num_socket);	
					break;	
				default:	
					printf("format message incorrect\n"	);
					break;
			}
		}

		if (k>0&&FD_ISSET(num_socket, &rfds)) {
			if(reception(pseudo,num_socket)==0) {
				h_close(num_socket);
				FD_CLR(num_socket,&rfdsbis);
			}
		}
		bcopy ( (char*) &rfdsbis, (char*) &rfds, sizeof(rfds)) ; /*set= ensemble initial */
	}

	printf("Au revoir, et à très bientôt.\n");	//thank you for using our app
}

/**
 * Il récupère et traite les messages reçus par le serveur
 * 
 * @param pseudo le pseudo de l'utilisateur
 * @param num_socket le numéro de prise
 */
int reception(char* pseudo,int num_socket) {
	char type[1];
	char nb_abonnement[1];
	char code_verif[1];
	char message[20];
	char* abonnement=malloc(6);
	if (h_reads(num_socket,type,1)==0) {
		return 0;
	}
	int typen=type[0];
	int code_verifn;
	switch (typen) {
		case Type_connect:
			h_reads(num_socket,code_verif,1);
			code_verifn=code_verif[0];
			switch (code_verifn) {
				case Respond_ok:
					printf("Vous êtes connecté avec le pseudo %s \n",pseudo);
					break;
				case Respond_Erreur:
					printf("La connection avec le pseudo %s a échoué\n",pseudo);
					break;
				default:
					break;
			}
			break;
		case Type_sub:
			h_reads(num_socket,code_verif,1);
			code_verifn=code_verif[0];
			switch (code_verifn) {
				case Respond_ok:
					printf("Vous êtes désormais abonné \n");
					break;
				case Respond_Erreur:
					printf("L'abonnement a échoué\n");
					break;
				default:
					break;
			}
			break;			

		case Type_unsub:
			h_reads(num_socket,code_verif,1);
			code_verifn=code_verif[0];
			switch (code_verifn) {
				case Respond_ok:
					printf("Vous êtes désormais plus abonné \n");
					break;
				case Respond_Erreur:
					printf("le désabonnement a échoué\n");
					break;
				default:
					break;
			}
			break;				

		case Type_list:
			h_reads(num_socket,code_verif,1);
			code_verifn=code_verif[0];
			switch (code_verifn) {
				case Respond_ok:
					h_reads(num_socket,nb_abonnement,1);
					printf("La liste de vos abonnement est composée de %d personne(s)\n",(int)nb_abonnement[0]);
					for(int i=0;i<(int)nb_abonnement[0];i++) {
						char abonnement[6];
						h_reads(num_socket,abonnement,6);
						printf("- %s ;\n",abonnement);

					}
					break;
				case Respond_Erreur:
					printf("la demande de liste a échoué\n");
					break;
				default:
					break;
			}
			break;			
		case Type_publish:
			h_reads(num_socket,abonnement,6);
			h_reads(num_socket,message,20);
			strcat(abonnement,"\0");
			printf("- %s : %s\n",abonnement,message);
			break;
		default:
			break;
	}
	free(abonnement);
	return 1;
}

/**
 * Il prend un type de message, un numéro de socket, un pseudo et une donnée, et envoie le message au
 * serveur
 * 
 * @param type le type de message à envoyer
 * @param num_socket le numéro de prise
 * @param pseudo le pseudo du client
 * @param data les données à envoyer
 */
int envoie(int type,int num_socket,char* pseudo,char* data) {
	int number=strlen(data);
	switch (type) {
		case Type_sub:
		case Type_unsub:
			if (number!=6) {
				return -1;
			}
			break;
		case Type_publish:
			if (number<1||number>20) {
				return -1;
			}
			number=20;
			break;
		default:
			break;
	}
	// on reformate le message pour l'envoyer
	char data_send [number+8];
	data_send[0]=(char)type;
	for(int i=0;i<6;i++) {
		data_send[i+1]=pseudo[i];
	}
	
	for(int i=0;i<strlen(data);i++) {
		data_send[i+7]=data[i];
	}
	data_send[strlen(data)+8]='\0';
	h_writes(num_socket,data_send,number+8);
	return 0;
}

/**
 * Il demande le pseudo à l'utilisateur, puis envoie une demande d'abonnement au serveur
 * 
 * @param num_socket le numéro de prise
 * @param pseudo le pseudo de l'utilisateur
 */
void follow(int num_socket,char* pseudo) {
	printf("Entrez le pseudo de la personne à laquelle vous voulez vous abonner\n");
	char* data=malloc(6);
	scanf("%s",data);

	while (envoie(Type_sub,num_socket,pseudo,data)==-1) {
		printf("le pseudo entrer est invalide\n");
		scanf("%s",data);
	}
}

/**
 * Il demande à l'utilisateur d'entrer un pseudo, puis envoie un message au serveur avec le type
 * Type_unsub, le pseudo de l'utilisateur, et le pseudo de l'utilisateur pour se désabonner de
 * 
 * @param num_socket le numéro de prise
 * @param pseudo le pseudo de l'utilisateur
 */
void unfollow(int num_socket,char* pseudo) {
	printf("Entrez le pseudo de la personne à laquelle vous voulez vous désabonner\n");
	char* data=malloc(6);
	scanf("%s",data);
	while (envoie(Type_unsub,num_socket,pseudo,data)==-1) {
		printf("le pseudo entrer est invalide\n");
		scanf("%s",data);
	}
}

/**
 * Il envoie une requête de liste au serveur
 * 
 * @param num_socket le numéro de prise
 * @param pseudo le pseudo de l'utilisateur
 */
void list_sub(int num_socket,char* pseudo) {
	envoie(Type_list,num_socket,pseudo,"");
}

/**
 * Il demande à l'utilisateur d'écrire un message, puis il l'envoie au serveur
 * 
 * @param num_socket le numéro de prise
 * @param pseudo le pseudo de l'utilisateur
 */
void publish_message(int num_socket,char* pseudo) {
	printf("Ecrivez le message que vous voulez publiez\n");
	char data[20];
	scanf("%[^\t\n]",data);
	while (envoie(Type_publish,num_socket,pseudo,data)==-1) {
		printf("le message écrit est invalide\n");
		scanf("%[^\t\n]",data);
	}
	printf("Message publié\n");
}

/**
 * Il demande à l'utilisateur de s'authentifier en entrant un pseudo, puis l'envoie au serveur
 * 
 * @param pseudo le pseudo de l'utilisateur
 * @param num_socket le numéro de prise
 */
void authentification(char* pseudo,int num_socket) {
	printf("Entrez votre pseudo: (6 caractères)\n");
	scanf("%s",pseudo);
	while(strlen(pseudo)!=6) {
		printf("votre pseudo est invalide:\n");
		scanf("%s",pseudo);
	}
	envoie(Type_connect,num_socket,pseudo,"");
}

/**
 * Il envoie un message de déconnexion au serveur
 * 
 * @param pseudo le pseudo de l'utilisateur qui veut se déconnecter
 * @param num_socket le numéro de prise
 */
void disconnect(char* pseudo,int num_socket) {
	envoie(Type_disconnect,num_socket,pseudo,"");
}

/* procedure correspondant au traitement du client de votre application */


/*****************************************************************************/

