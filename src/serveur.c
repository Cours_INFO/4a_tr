/******************************************************************************/
/*			Application: flash			              */
/******************************************************************************/
/*									      */
/*			 programme  SERVEUR 				      */
/*									      */
/******************************************************************************/
/*									      */
/*		Auteurs : Bonfils Antoine, Ferrari Julien, Thomazo Corentin*/
/*		Date :  9 décembre 2022						      */
/*									      */
/******************************************************************************/	

#include <stdio.h>
#include <curses.h>
#include <string.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <stdlib.h>

#include "../lib/fon.h"  
#include "../lib/data_struct.h"
   		/* Primitives de la boite a outils */

#define SERVICE_DEFAUT "1111"

#define Respond_ok 48
#define Respond_okS "0"

#define Respond_Erreur 49
#define Respond_ErreurS "1"



#define Type_sub 48
#define Type_unsub 49
#define Type_list 50
#define Type_publish 51
#define Type_connect 52
#define Type_disconnect 53
#define Type_mew_message 54

void serveur_appli (char *service);   /* programme serveur */
void run(int num_socket);
void ajout_liste_abonne();
pro* verifyinit(char* pseudo,liste_pro* lp);
void parcourlistabo(char* data_send,pro* profile,int nb_abo);
int envoie(int num_socket,char* data,int code,pro* profile);
void publish(pro* p,msg* m);
liste_msg* envoie_message(int num_socket ,pro* p);

pro* verifyinit(char* pseudo,liste_pro* lp);


/******************************************************************************/	
/*---------------- programme serveur ------------------------------*/

int main(int argc,char *argv[])
{

	char *service= SERVICE_DEFAUT; /* numero de service par defaut */


	/* Permet de passer un nombre de parametre variable a l'executable */
	switch (argc)
 	{
   	case 1:
		  printf("defaut service = %s\n", service);
		  		  break;
 	case 2:
		  service=argv[1];
            break;

   	default :
		  printf("Usage:serveur service (nom ou port) \n");
		  exit(1);
 	}

	/* service est le service (ou numero de port) auquel sera affecte
	ce serveur*/
	
	serveur_appli(service);
}


/******************************************************************************/	
/**
 * Il crée un socket, le lie à un port, l'écoute, puis attend les connexions. 
 * Lorsqu'une connexion est établie, il ajoute le socket à une liste de sockets, 
 * puis attend que les données soient envoyées.
 * 
 * Lorsque des données sont envoyées, il lit les données, puis fait quelques chose en fonction du type
 * de données envoyées
 * 
 * @param service le nom du service à utiliser.
 */
void serveur_appli(char *service) {

	/*Initialisation des listes*/
	// liste profile
	liste_pro* lp=NULL;
	//liste socket
	liste_soc* ls=NULL;

	void* adr_serv;
	//char data[128];
	struct sockaddr_in* p_adr_serv=adr_serv;
	int num_socket_passive=0 ;
		adr_socket(service,NULL,SOCK_STREAM,&p_adr_serv);
		num_socket_passive =h_socket(AF_INET,SOCK_STREAM);
		if (num_socket_passive==-1) {
			return ;
		}
	h_bind(num_socket_passive,p_adr_serv);

	h_listen(num_socket_passive,5);
	fd_set rfds;
	fd_set rfdsbis;
	FD_ZERO(&rfds);
	FD_SET(num_socket_passive, &rfds);
	bcopy ( (char*) &rfds, (char*) &rfdsbis, sizeof(rfds)) ;
	while (true) {
		liste_soc* del_liste=NULL;
		int k =select (FD_SETSIZE,&rfds, 0, 0, 0) ;
		if(k>0) {
			/*gère l'arrivée de nouvelle socket avec socket passive*/
			if (FD_ISSET(num_socket_passive, &rfds)) {
				int num_socket=h_accept(num_socket_passive,p_adr_serv);
				add_list_soc(create_soc(num_socket),&ls);
				FD_SET(num_socket,&rfdsbis);
			}
			liste_soc* l=ls;
			/*gère les socket connecté au client*/
			while(l!=NULL&&l->socket!=NULL) {
				soc* socket=l->socket;
				
				int num_socket=l->socket->id_socket;
				printf("num_socket=%d\n",num_socket);
				pro* profile=NULL;
				if (l->socket->profile!=NULL) {
					profile=l->socket->profile;
				}
				if (FD_ISSET(num_socket, &rfds)) {
					char data[1];
					char pseudo[6];
					char pseudopers[6];
					char message[20];
					int code=1;
					int code2;
					int est_deco=0;
					if (0==h_reads(num_socket,data,1)) {
						data[0]=Type_disconnect;
						est_deco=1;
					}
					switch (data[0]) {
						case Type_connect:
						// attribue à la socket un profile
							h_reads(num_socket,pseudo,6);
							pro* newprofile=verifyinit(pseudo,lp); //attribue à profile le pseudo
							add_list_pro(newprofile,&lp);
							l->socket->profile=newprofile;
							envoie(num_socket,data,code,newprofile);

							break;
						case Type_sub:
						// abonne le profile à sub et l'ajoute à abonnement
							h_reads(num_socket,pseudo,6); 

							h_reads(num_socket,pseudopers,6);
							pro* sub=contain_list_pro(pseudopers,lp);
							if (sub!=NULL&&contain_list_pro(pseudopers,profile->abonnements)==NULL) {								
							
								add_list_pro(sub,&(l->socket->profile->abonnements));
								add_list_pro(l->socket->profile,&(sub->abonnes));
								l->socket->profile->nbAbonnement=l->socket->profile->nbAbonnement+1;
									code=1;
							
							}else{
								code = 0;
							}
							envoie(num_socket,data,code,profile);
							break;
						case Type_unsub:
						// desabonne le profile à sub et l'enlève à abonnement
							h_reads(num_socket,pseudo,6); 
							h_reads(num_socket,pseudopers,6); 
							
							pro* unsub=contain_list_pro(pseudopers,lp);
							if (unsub!=NULL&&contain_list_pro(pseudopers,profile->abonnements)!=NULL) {								
								l->socket->profile->abonnements=rm_list_pro(unsub,&(l->socket->profile->abonnements));
								unsub->abonnes=rm_list_pro(l->socket->profile,&(unsub->abonnes));
								l->socket->profile->nbAbonnement=l->socket->profile->nbAbonnement-1;
								code=1;
							}else{
								code = 0;
							}
							envoie(num_socket,data,code,profile);
							break;
						case Type_list:
							h_reads(num_socket,pseudo,6); 

							envoie(num_socket,data,1,profile);
							break;
						case Type_publish:
							h_reads(num_socket,pseudo,6); 

							h_reads(num_socket,message,21); 
							msg* msg=create_msg(profile->nbAbonnement,pseudo,message);
							publish(profile,msg);	
							break;
						case Type_disconnect:
							if(!est_deco) {
							h_reads(num_socket,data,6);
							}
							soc* socket= l->socket;
							add_list_soc(socket,&del_liste);
							FD_CLR(num_socket,&rfdsbis);

							printf("test1\n");
							break;
						default:
							break;
					}
				}
				if (l!=NULL) {
					l=l->suivant;
				}
			}
			while(del_liste!=NULL) {
				if(del_liste->socket!=NULL) {
					int num_socket=del_liste->socket->id_socket;
					soc* socket1=del_liste->socket;
					ls=rm_list_soc(del_liste->socket,&ls);
					delete_soc(socket1);
					h_close(num_socket);
					FD_CLR(num_socket,&rfdsbis);
				}
				del_liste=del_liste->suivant;
			}
			l=ls;
			while(l!=NULL) {
				int num_socket2=l->socket->id_socket;
				pro* profile2=l->socket->profile;
				if(profile2!=NULL) {	
					profile2->messagerie=envoie_message(num_socket2,profile2);
				}
				l=l->suivant;
			}
		}
		bcopy ( (char*) &rfdsbis, (char*) &rfds, sizeof(rfds)) ; /*set= ensemble initial */
	}	
}

/**
 * Il envoie une réponse/un message au client
 * 
 * @param num_socket le numéro de prise
 * @param data les données reçues du client
 * @param code 1 si la requête a réussi, 0 si elle a échoué
 * @param profile le profil de l'utilisateur
 */
int envoie(int num_socket,char* data,int code,pro* profile) {
	int nb_abo=profile->nbAbonnement;
	char* data_send;
	char c=nb_abo;

	switch (data[0]) {
		case Type_sub:
		case Type_unsub:
		case Type_connect:
			data_send=malloc(2);
			data_send[0]=data[0];
			if (code) {
				data_send[1]=(char)Respond_ok;
				h_writes(num_socket,data_send,2);
			}else{
				data_send[1]=Respond_Erreur;
				h_writes(num_socket,data_send,2);
			}
			break;
		case Type_list:
			data_send=malloc(3+6*nb_abo);
			data_send[0]=data[0];
			data_send[1]=Respond_ok;
			data_send[2]=(char) c;
			parcourlistabo(data_send,profile,nb_abo);
			printf("%s\n",data_send);
			h_writes(num_socket,data_send,3+nb_abo*6);
		default:
			break;
	}
}

/**
 * Il gère l'attribution et la création de profil; si un profil existe, sinon il le crée.
 * 
 * @param pseudo le nom du profil
 * @param lp la liste des profils
 * 
 * @return Un pointeur vers un profil.
 */
pro* verifyinit(char* pseudo,liste_pro* lp) {
	pro* newprofile = contain_list_pro( pseudo, lp);

	if (newprofile==NULL) {
		newprofile=create_pro(pseudo);
	}
	return newprofile;
}

/**
 * Il prend une chaîne, un profil et un entier comme paramètres, et il remplit la chaîne avec le pseudo
 * des abonnements du profil
 * 
 * @param data_send la chaîne qui sera envoyée au client
 * @param profile le profil de l'utilisateur qui est connecté
 * @param nb_abo le nombre d'abonnements
 */
void parcourlistabo(char* data_send,pro* profile,int nb_abo) {
	liste_pro* l=profile->abonnements;
	for (int i=0;i<nb_abo;i++) {
		for(int j=0;j<6;j++) {
			data_send[3+j+6*i]=l->profil->pseudo[j];
		}
		l=l->suivant;
	}
}

/**
 * Il ajoute le message à la boîte de réception de tous les abonnés du profil
 * 
 * @param profile le profil de l'utilisateur qui publie le message
 * @param msg le message à publier
 */
void publish(pro* profile,msg* msg) {
	liste_pro* l=profile->abonnes;
	while(l!=NULL) {
		add_list_msg(msg,&(l->profil->messagerie));
		l=l->suivant;
	}
}	

/**
 * Il envoie les messages de l'utilisateur au client
 * 
 * @param num_socket le numéro de prise
 * @param profile le profil de l'utilisateur
 * 
 * @return une liste de messages.
 */
liste_msg* envoie_message(int num_socket,pro* profile) {
	liste_msg* msg1= profile->messagerie;
	while(msg1!=NULL) {
		char* data_send=malloc(27);
		char c=Type_publish;
		data_send[0]=(char)c;
		for(int i=0;i<6;i++) {
			data_send[1+i]=msg1->m->pseudo[i];
		}
		for(int i=0;i<20;i++) {
			data_send[7+i]=msg1->m->requete[i];
		}
		
		h_writes(num_socket,data_send,27);
		msg* message=msg1->m;
		message->nb=message->nb-1;
		msg1=rm_list_msg(message,&msg1);
		if (message->nb==0) {
			delete_msg(message);
		}
	}
	return msg1;
}


