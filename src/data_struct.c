#include "../lib/data_struct.h"


/* FONCTIONS MESSAGE */

msg* create_msg(int nb, char* pseudo, char* requete){
    msg* message = malloc(sizeof(msg));
    message->nb = nb;
    message->pseudo = pseudo;
    message->requete = requete;
}

void delete_msg(msg* message){
    free(message);
}

liste_msg* add_list_msg(msg* message, liste_msg** liste_message){
    liste_msg *elem = malloc(sizeof(liste_message));
	if(!elem) exit(EXIT_FAILURE);
	elem->m = message;
	elem->suivant = *liste_message;
	*liste_message = elem;
}

liste_msg* rm_list_msg(msg* message, liste_msg** liste_message){
	liste_msg *temp;
    msg *message_temp;
    while(*liste_message){
		if(strcmp((*liste_message)->m->requete, message->requete)==0){
			temp = (*liste_message)->suivant;
	        message_temp = (*liste_message)->m;
	        free(*liste_message);
	        *liste_message = temp;
	        return *liste_message;
		}
		*liste_message = (*liste_message)->suivant;
	}   
}

int est_liste_msg_vide(liste_msg* liste_message){
    if(!liste_message){
        return 1;
    }
    return 0;
}


/* FONCTIONS PROFIL*/

pro* create_pro(char* pseudo){
    pro* new_profile = malloc(sizeof(pro));
    new_profile->pseudo=malloc(6);
    strcpy(new_profile->pseudo,pseudo);
    new_profile->nbAbonnement = 0;
    new_profile->abonnements = NULL;
    new_profile->abonnes = NULL;
    new_profile->messagerie = NULL;
}

liste_pro* add_list_pro(pro* profile, liste_pro** liste_profile){
    liste_pro *elem = malloc(sizeof(liste_profile));
	if(!elem) exit(EXIT_FAILURE);
	elem->profil = profile;
	elem->suivant = *liste_profile;
	*liste_profile = elem;

}

liste_pro* rm_list_pro(pro* profile, liste_pro** liste_profile){
    liste_pro *temp;
    pro *profile_temp;
    while(*liste_profile != NULL){
		if(strncmp((*liste_profile)->profil->pseudo, profile->pseudo,6)==0){
			temp = (*liste_profile)->suivant;
	        profile_temp = (*liste_profile)->profil;
	        free(*liste_profile);
	        *liste_profile = temp;
	        return *liste_profile;
		}
		*liste_profile = (*liste_profile)->suivant;
	}  

}

pro* contain_list_pro(char* pseudo, liste_pro* liste_profile){
    while(liste_profile){
		if(strncmp(liste_profile->profil->pseudo, pseudo,6)==0){
			return liste_profile->profil;
		}else
        {
            liste_profile = liste_profile->suivant;
        }
	}
	return NULL;
}

int est_liste_pro_vide(liste_pro* liste_profile){
    if(!liste_profile){
        return 1;
    }
    return 0;
}


/* FONCTIONS SOCKET */

soc* create_soc(int id){
    soc* socket = malloc(sizeof(soc));
    socket->id_socket = id;
    socket->profile = NULL;
}

void delete_soc(soc* socket){
    free(socket);
}

liste_soc* add_list_soc(soc* socket, liste_soc** liste_socket){
    liste_soc *elem = malloc(sizeof(liste_soc));
	if(!elem) exit(EXIT_FAILURE);
	elem->socket = socket;
	elem->suivant = *liste_socket;
	*liste_socket = elem;
}

liste_soc* rm_list_soc(soc* socket1, liste_soc** liste_socket){
    liste_soc *temp;
    soc *socket_temp;
    while(*liste_socket != NULL){
		if((*liste_socket)->socket->id_socket == socket1->id_socket){
			temp = (*liste_socket)->suivant;
	        socket_temp = (*liste_socket)->socket;
	        free(*liste_socket);
	        *liste_socket = temp;
	        return *liste_socket;
		}
		*liste_socket = (*liste_socket)->suivant;
	}
}

int est_liste_soc_vide(liste_soc* liste_socket){
    if(liste_socket == NULL){
        return 1;
    }
    return 0;
}


/* DEBUT TESTS */

//int main(int argc, char *argv[]){
    
/* DEBUT TESTS MESSAGE */
/*
    liste_msg *lm = NULL;

    msg *m0 = create_msg(1,"Tototo","Coucou");
    msg *m1 = create_msg(1,"Tititi","Bonjour");
    msg *m2 = create_msg(2,"Tututu","Salut");
    msg *m3 = create_msg(3,"Tatata","Au revoir");

    if(est_liste_msg_vide(lm)==1){
        printf("SUCCES\n");
    }else{
        printf("ECHEC\n");
    }

    add_list_msg(m0,(&lm));
    add_list_msg(m1,(&lm));
    add_list_msg(m2,(&lm));

    if(est_liste_msg_vide(lm)==0){
        printf("SUCCES\n");
    }else{
        printf("ECHEC\n");
    }

    rm_list_msg(m3,(&lm)); ///Test retire d'un élément non présent des la liste

    rm_list_msg(m0,(&lm));
    rm_list_msg(m1,(&lm));
    rm_list_msg(m2,(&lm));

    if(est_liste_msg_vide(lm)==1){
        printf("SUCCES\n");
    }else{
        printf("ECHEC\n");
    }

    rm_list_msg(m1,(&lm)); // Test retire d'une liste vide

    delete_msg(m0);
    delete_msg(m1);
    delete_msg(m2);

    printf("\n");
*/
/* FIN TESTS MESSAGE */

/* DEBUT TESTS PROFIL */
/*
    liste_pro *lp = NULL;

    pro *p0 = create_pro("Tototo");
    pro *p1 = create_pro("Tititi");
    pro *p2 = create_pro("Tututu");
    pro *p3 = create_pro("Tatata");

    if(est_liste_pro_vide(lp)==1){
        printf("SUCCES\n");
    }else{
        printf("ECHEC\n");
    }

    add_list_pro(p0,(&lp));
    add_list_pro(p1,(&lp));
    add_list_pro(p2,(&lp));

    if(est_liste_pro_vide(lp)==0){
        printf("SUCCES\n");
    }else{
        printf("ECHEC\n");
    }

    rm_list_pro(p3,(&lp));

    if(contain_list_pro("Tututu",lp)!=NULL){
        printf("SUCCES\n");
    }else{
        printf("ECHEC\n");
    }

    if(contain_list_pro("Tatata",lp)==NULL){
        printf("SUCCES\n");
    }else{
        printf("ECHEC\n");
    }

    rm_list_pro(p2,(&lp));
    rm_list_pro(p1,(&lp));
    rm_list_pro(p0,(&lp));

    if(est_liste_pro_vide(lp)==1){
        printf("SUCCES\n");
    }else{
        printf("ECHEC\n");
    }
    
    rm_list_pro(p0,(&lp));

    printf("\n");
*/
/* FIN TESTS PROFIL */

/* DEBUT TESTS SOCKET */
/*
    liste_soc *ls = NULL;

    soc *s0 = create_soc(0);
    soc *s1 = create_soc(1);
    soc *s2 = create_soc(2);
    soc *s3 = create_soc(3);

    if(est_liste_soc_vide(ls)==1){
        printf("SUCCES\n");
    }else{
        printf("ECHEC\n");
    }

    add_list_soc(s0,(&ls));
    add_list_soc(s1,(&ls));
    add_list_soc(s2,(&ls));

    if(est_liste_soc_vide(ls)==0){
        printf("SUCCES\n");
    }else{
        printf("ECHEC\n");
    }

    rm_list_soc(s3,(&ls)); ///Test retire d'un élément non présent des la liste

    rm_list_soc(s0,(&ls));
    rm_list_soc(s1,(&ls));
    rm_list_soc(s2,(&ls));

    if(est_liste_soc_vide(ls)==1){
        printf("SUCCES\n");
    }else{
        printf("ECHEC\n");
    }

    rm_list_soc(s1,(&ls)); // Test retire d'une liste vide

    delete_soc(s0);
    delete_soc(s1);
    delete_soc(s2);

    printf("\n");
*/

/* FIN TESTS SOCKET */

//}

/* FIN TESTS */