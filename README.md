# TP SOCKET : Programmation d’une application d’échange de messages courts en utilisant les sockets

Travail Pratique dans le cadre du cours de Technologies des Réseaux à **POLYTECH GRENOBLE**.

***

## Remarques du professeur:

### **Remarque**

```TXT
Derniere version des fonctions associees aux sockets.

makefile permettant de compiler sous differents systemes.
Utiliser gmake et non make.

Si probleme lors de l'execution avec ucblib, rajouter: setenv LD_LIBRARY /usr/ucblib/

Essaicurses et le make associe makecurse permettent de comprendre les primitives de gestions de curseur dans plusieurs fenetres. Permet de faire un joli TALK.
```

### **makecurse**
```TXT
essaicurse: 
	gcc EssaiCurses.c -o essaicurse  -L/usr/ccs/lib -L/usr/ucblib  -lcurses -ltermcap
```

## Sujet

voir l'[énoncé](./docs/tpflash.pdf).

## Compte-rendu

 * Le [code source](./src/) du programme client et serveur fortement commenté.
 * Une [documentation annexe](#documentation) sur le code si cela vous semble nécessaire.
 * Éventuellement les [restrictions](#restrictions) de vos programmes par rapport au cahier des charges initial.
 * Des jeux de [tests](#tests-effectués).
 * Éventuellement des commentaires sur les problèmes rencontrés.

 ### Documentation

Utilisateur: `flash-client mandelbrot 9999`

 * pseudo à 6 caractères (création de son compte "Flash")
 * peut:
    * s’abonner à un compte
    * se désabonner à un compte
    * lister tous les pseudos de ces abonnements
    * publier un message inférieur à 20 caractères à ses abonnées
    * quitter l’application
 * à la connexion, l’utilisateur reçoit tous les messages publiés par les comptes auxquels il est abonné. Pour chaque message, le pseudo de l’émetteur est affiché.
 * le serveur mémorise les messages envoyés par un utilisateur (si ce dernier est suivi par au moins une personne). Les messages sont mémorisés jusqu’à que tous les abonnés l’ai reçu.

Serveur: `flash-serveur 9999`

### **Restrictions**

#### **La partie client** 
l'application se connecte a un serveur.
On utilise la détection des périphérique d'entrée pour intérragir avec l'application 
les touches pour utiliser l'application:
- p pour publier un message
- s pour vous abonner 
- d pour vous désabonner 
- l pour afficher la liste des abonnements 
- q pour quitter l'application


#### **La partie serveur** 

Elle utilise une structure de donnée spécifique (data_struct)

elle stocke les données en ram et non en dure. 
Le serveur est monothreadé et uilise slect pour gérer plusieurs socket

#### Pour aller plus loin:
Nous aurons aimé implémenter un moyen de sauvegarder les données du serveur.


### **Tests effectués**:

- connexion de plusieurs clients

- s'abonner à un profil existant (connexter ou déconnecter) [OK]
- s'abonner à une personne qui n'existe pas [OK]
- s'abonner à une personne dont on est déja abonné [OK]

- se désabonner à un profil existant (connexter ou déconnecter) [OK]
- se désabonner à une personne qui n'existe pas [OK]
- se désabonner à une personne dont on est déja abonné [OK]

- affichage d'une liste des abonné avec 0 ou plusieurs personnes [OK]

- publication de message [OK]
- stokage des message pendant une déconnection [OK]
- envoie des message lors de la connection [OK]

- résistance du serveur à un crash d'un client [OK] 