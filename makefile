OBJDIR = bin

OBJ1 = fon.o data_struct.o client.o
OBJ2 = fon.o data_struct.o serveur.o
OPTIONS	=
# Adaptation a Darwin / MacOS X avec fink
# Du fait de l'absence de libtermcap on se fait pas mal
# cracher dessus mais ca marche...
ifeq ($(shell uname), Darwin)
LFLAGS	+= -L/opt/local/lib
CFLAGS	+= -I /opt/local/include
endif
#Changer si necessaire le chemin d'acces aux librairies

# Adaptation a Linux
ifeq ($(shell uname), Linux)
OPTIONS	+= -ltermcap
endif

# Adaptation a FreeBSD
# Attention : il faut utiliser gmake...
ifeq ($(shell uname),FreeBSD)
OPTIONS	+= -ltermcap
endif

# Adaptation a Solaris

ifeq ($(shell uname),SunOS)
OPTIONS	+= -ltermcap  -lsocket -lnsl
CFLAGS	+= -I..
endif

EXEC = ${OBJ1} client ${OBJ2} serveur
all: directory ${EXEC} move

directory :
	mkdir -p $(OBJDIR)

move :
	mv -t $(OBJDIR) ${OBJ1} serveur.o

data_struct.o : lib/data_struct.h src/data_struct.c
	gcc -c src/data_struct.c
	#gcc -c src/data_struct.c

fon.o :  lib/fon.h src/fon.c
	gcc -c src/fon.c
	#gcc -c src/fon.c

client.o : lib/fon.h	lib/data_struct.h	src/client.c 
	gcc  $(CFLAGS) -c  src/client.c	

serveur.o : lib/fon.h	lib/data_struct.h	src/serveur.c 
	gcc  $(CFLAGS) -c  src/serveur.c	

client : ${OBJ1}	
	gcc $(LFLAGS) ${OBJ1} -o client -lcurses   $(OPTIONS)

serveur : ${OBJ2}	
	gcc $(LFLAGS) ${OBJ2} -o serveur -lcurses   $(OPTIONS)



clean : 
	rm -rf ${EXEC} core $(OBJDIR)

