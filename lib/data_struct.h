#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#ifndef DATA_STRUCT_H
#define  DATA_STRUCT_H

/* DEBUT STRUCTURES*/

/* STRUCTURES MESSAGE */

typedef struct message{
    int nb;
    char* pseudo;
    char* requete;
}msg;

typedef struct liste_message{
    msg* m;
    struct liste_message *suivant;
}liste_msg;

/* STRUCTURES PROFIL */

typedef struct liste_profil liste_pro;

typedef struct profil{
    char* pseudo;
    int nbAbonnement;
    liste_pro* abonnes;
    liste_pro* abonnements;
    liste_msg* messagerie;
}pro;

struct liste_profil{
    pro* profil;
    liste_pro* suivant;
};

/* STRUCTURES SOCKET */

typedef struct socket{
    int id_socket;
    pro* profile;
}soc;

typedef struct liste_socket{
    soc* socket;
    struct liste_socket* suivant;
}liste_soc;

/* FIN STRUCTURES */


/* DEBUT PRIMITIVES */

/* PRIMITIVES MESSAGE */

msg* create_msg(int nb, char* pseudo, char* requete);
void delete_msg(msg* message);

liste_msg* add_list_msg(msg* message, liste_msg** liste_message);
liste_msg* rm_list_msg(msg* message, liste_msg** liste_message);

int est_liste_msg_vide(liste_msg* liste_message);

/* PRIMITIVES PROFIL */

pro* create_pro(char* pseudo);

liste_pro* add_list_pro(pro* profile, liste_pro** liste_profile);
liste_pro* rm_list_pro(pro* profile, liste_pro** liste_profile);

pro* contain_list_pro(char* pseudo , liste_pro* liste_profile);
int est_liste_pro_vide(liste_pro* liste_profile);

/* PRIMITIVES SOCKET */

soc* create_soc(int id);
void delete_soc(soc *socket);

liste_soc* add_list_soc(soc* socket, liste_soc** liste_socket);
liste_soc* rm_list_soc(soc* socket, liste_soc** liste_socket);

int est_liste_soc_vide(liste_soc* liste_socket);

/* FIN PRIMITIVES */

#endif